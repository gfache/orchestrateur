package fr.afcepf.al33.dtoClients;

import java.io.Serializable;
import java.text.SimpleDateFormat;

public class ClientDTO extends UtilisateurDTO implements Serializable {
	private static final long serialVersionUID = 1L;

    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    public ClientDTO() {
    }

    private String numTelPortable;
    private String numTelFixe;
    private String motifSuspension;
    private String adresse;
    private String ville;
    private String codePostal;
    private String role;
    private String dateNaissance;
    private String dateInscription;
    private String dateDesinscription;
    private String dateDebutSuspension;
    private String dateFinSuspension;

    public String getNumTelPortable() {
        return numTelPortable;
    }

    public void setNumTelPortable(String numTelPortable) {
        this.numTelPortable = numTelPortable;
    }

    public String getNumTelFixe() {
        return numTelFixe;
    }

    public void setNumTelFixe(String numTelFixe) {
        this.numTelFixe = numTelFixe;
    }

    public String getMotifSuspension() {
        return motifSuspension;
    }

    public void setMotifSuspension(String motifSuspension) {
        this.motifSuspension = motifSuspension;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(String dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public String getDateInscription() {
        return dateInscription;
    }

    public void setDateInscription(String dateInscription) {
        this.dateInscription = dateInscription;
    }

    public String getDateDesinscription() {
        return dateDesinscription;
    }

    public void setDateDesinscription(String dateDesinscription) {
        this.dateDesinscription = dateDesinscription;
    }

    public String getDateDebutSuspension() {
        return dateDebutSuspension;
    }

    public void setDateDebutSuspension(String dateDebutSuspension) {
        this.dateDebutSuspension = dateDebutSuspension;
    }

    public String getDateFinSuspension() {
        return dateFinSuspension;
    }

    public void setDateFinSuspension(String dateFinSuspension) {
        this.dateFinSuspension = dateFinSuspension;
    }

    @Override
    public String toString() {
        return "ClientDTO{" +
                "numTelPortable='" + numTelPortable + '\'' +
                ", numTelFixe='" + numTelFixe + '\'' +
                ", motifSuspension='" + motifSuspension + '\'' +
                ", adresse='" + adresse + '\'' +
                ", ville='" + ville + '\'' +
                ", codePostal='" + codePostal + '\'' +
                ", role='" + role + '\'' +
                ", dateNaissance='" + dateNaissance + '\'' +
                ", dateInscription='" + dateInscription + '\'' +
                ", dateDesinscription='" + dateDesinscription + '\'' +
                ", dateDebutSuspension='" + dateDebutSuspension + '\'' +
                ", dateFinSuspension='" + dateFinSuspension + '\'' +
                ", idUtilisateur=" + idUtilisateur +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", email='" + email + '\'' +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", civilite=" + civilite +
                '}';
    }
}