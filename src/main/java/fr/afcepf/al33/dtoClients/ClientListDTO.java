package fr.afcepf.al33.dtoClients;

import java.io.Serializable;
import java.util.List;

public class ClientListDTO implements Serializable {
	private static final long serialVersionUID = 1L;

    public ClientListDTO() {
    }

    public ClientListDTO(List<ClientDTO> clients) {
        this.clients = clients;
    }

    private List<ClientDTO> clients;

    public List<ClientDTO> getClients() {
        return clients;
    }

    public void setClients(List<ClientDTO> clients) {
        this.clients = clients;
    }
}
