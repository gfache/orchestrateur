package fr.afcepf.al33.dto;



import java.io.Serializable;

import fr.afcepf.al33.dtoClients.ClientDTO;

public class ModificationClientDTO implements Serializable {
	private static final long serialVersionUID = 1L;

    public ModificationClientDTO() {
    }

    private ClientDTO clientDTO;
    private String messageError;

    public ClientDTO getClientDTO() {
        return clientDTO;
    }

    public void setClientDTO(ClientDTO clientDTO) {
        this.clientDTO = clientDTO;
    }

    public String getMessageError() {
        return messageError;
    }

    public void setMessageError(String messageError) {
        this.messageError = messageError;
    }
}
