package fr.afcepf.al33.dto;

import java.io.Serializable;

public class AuthentificationRoleDTO implements Serializable {
	private static final long serialVersionUID = 1L;

    public AuthentificationRoleDTO() {
    }

    public AuthentificationRoleDTO(Integer id, String role, boolean isAuth) {
        this.id = id;
        this.role = role;
        this.isAuth = isAuth;
    }

    private Integer id;
    private String role;
    private boolean isAuth;

    public boolean isAuth() {
        return isAuth;
    }

    public void setAuth(boolean auth) {
        isAuth = auth;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

}
